<?php

namespace App\Models;
use CodeIgniter\Model;

class Article extends Model
{

    private $_db;

    public function __construct()
    {
        $this->_db = db_connect();
        return $this->_db;
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function create($data)
    {
        return ($this->_db->table('article')->insert($data)) ? $this->_db->insertId() : false;
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function modify($idArticle, $data)
    {
        $query = $this->_db->table('article')->where('id', $idArticle);
        $result = $query->update($data);
        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function deleteArticle($idArticle)
    {
        $result = $this->_db->table('article')->delete(['id' => $idArticle]);
        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function getAll()
    {
        $query = $this->_db->table('article')->get();
        return $query->getResult();
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function getSingle($id)
    {
        $query = $this->_db->table('article');
        $query->select('article.id, article.title, article.desc, article.price, article.extension, article.create_at, user.pseudo, user.mail, user.tel, user.checked, region.name');
        $query->join('user', 'user.id = article.id_user', 'INNER');
        $query->join('region', 'user.id_region = region.id', 'LEFT');
        $result = $query->getWhere(['article.id' => $id]);
        return $result->getRowObject();
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function getAllByCategory($idCategory)
    {
        $query = $this->_db->table('article')->getWhere(array('id_categorie' => $idCategory));
        return $query->getResult();
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function getTop()
    {
        $query = $this->_db->table('article')->orderBy('title', 'RANDOM')->limit(3)->get();
        return $query->getResult();
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function getAllByIdUser($idUser)
    {
        $query = $this->_db->table('article')->getWhere(array('id_user' => $idUser));
        return $query->getResult();
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function getAllBySearch($idCategory, $articleName, $minPrice, $maxPrice, $idRegion)
    {
        $query = $this->_db->table('article');
        $query->select('article.id, article.title, article.desc, article.price, article.extension, article.create_at');
        $query->join('user', 'user.id = article.id_user', 'INNER');
        $query->where('article.id_categorie', $idCategory);
        ($articleName != 0) ? $query->like(['article.title' => $articleName]) : '';
        ($minPrice != 0) ? $query->where('article.price >=', $minPrice) : '';
        ($maxPrice != 0) ? $query->where('article.price <=', $maxPrice) : '';
        ($idRegion != 0) ? $query->where('user.id_region', $idRegion) : '';

        $result = $query->get();

        return $result->getResult();
    }
}