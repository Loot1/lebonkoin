<?php

namespace App\Models;
use CodeIgniter\Model;

class Categories extends Model
{

    private $_db;

    public function __construct()
    {
        $this->_db = db_connect();
        return $this->_db;
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function getAll()
    {
        $query = $this->_db->table('categories')->get();
        return $query->getResult();
    }
}