<?php

namespace App\Models;
use CodeIgniter\Model;

class Region extends Model
{

    private $_db;

    public function __construct()
    {
        $this->_db = db_connect();
        return $this->_db;
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function getAll()
    {
        $query = $this->_db->table('region')->get();
        return $query->getResult();
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function getOne($id)
    {
        $query = $this->_db->table('region')->getWhere(array('id' => $id));
        return $query->getRowObject();
    }
}