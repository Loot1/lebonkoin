<?php

namespace App\Models;

use CodeIgniter\Model;

class User extends Model
{

    private $_db;
    
    public function __construct()
    {
        $this->_db = db_connect();
        return $this->_db;
    }
    
    // This function need to sign up user
    public function signUp($user)
    {
        return $this->_db->table("user")->insert($user);
    }
    
    // This function need to sign in user
    public function signIn($email)
    {
        $query = $this->_db->table("user")->getWhere(array("mail" => $email));
        return $query->getRowObject();
    }

    // This function need to reset password
    public function forgotPassword()
    {
    }

    // Function allowing verify if email exist in db
    public function verifyIfEmailExist($email)
    {
        $query = $this->_db->table("user")->getWhere(array("mail" => $email));
        return $query->getRowObject();
    }

    // Function allowing verify if pseudo exist in db
    public function verifyPseudoExist($pseudo)
    {
        $query = $this->_db->table("user")->getWhere(array("pseudo" => $pseudo));
        return $query->getRowObject();
    }

    // Function allowing verify token
    public function verifyToken($token)
    {
        $query = $this->_db->table("user")->getWhere(array("token" => $token));
        return $query->getRowObject();
    }

    // Function allowing modify 
    public function modify($table, $col, $where, $data)
    {
        $query = $this->_db->table($table)->where($col, $where);
        $result = $query->update($data);
        return $result;
    }

    // Function allowing get one user
    public function getOne($id)
    {
        $query = $this->_db->table('user')->getWhere(array('id' => $id));
        return $query->getRowObject();
    }

    // Function allowing select user
    public function select($table, $col, $where){
        $query = $this->_db->table($table)->getWhere(array($col => $where));
        return $query->getRowObject();
    }

    public function getAll()
    {
        $query = $this->_db->table('user')->get();
        return $query->getResult();
    }

    public function deleteUser($id)
    {
        $query = $this->_db->table('user')->where('id', $id);
        $query->delete();
    }
}
