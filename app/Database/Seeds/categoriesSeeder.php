<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class categoriesSeeder extends Seeder
{
        public function run()
        {
                $dataArray = [
                    ['type'    => 'Voiture'],
                    ['type'    => 'Vêtement'],
                    ['type'    => 'Electroménager'],
                    ['type'    => 'Meuble'],
                    ['type'    => 'Décoration'],
                    ['type'    => 'Livres'],
                    ['type'    => 'Immobilier'],
                    ['type'    => 'Autres']
                ];

                foreach ($dataArray as $data) {
                    // Using Query Builder
                    $this->db->table('categories')->insert($data);
                }
        }
}