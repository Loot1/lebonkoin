<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class regionSeeder extends Seeder
{
        public function run()
        {
                $dataArray = [
                    ['name'    => 'Guadeloupe'],
                    ['name'    => 'Martinique'],
                    ['name'    => 'Guyane'],
                    ['name'    => 'La Réunion'],
                    ['name'    => 'Mayotte'],
                    ['name'    => 'île-de-France'],
                    ['name'    => 'Centre-Val de Loire'],
                    ['name'    => 'Bourgogne-Franche-Comté'],
                    ['name'    => 'Hauts-de-France'],
                    ['name'    => 'Grand Est'],
                    ['name'    => 'Bretagne'],
                    ['name'    => 'Nouvelle-Aquitaine'],
                    ['name'    => 'Occitanie'],
                    ['name'    => 'Auvergne-Rhône-Alpes'],
                    ['name'    => 'Provence-Alpes-Côte d\'Azur'],
                    ['name'    => 'Corse']
                ];

                foreach ($dataArray as $data) {
                    // Using Query Builder
                    $this->db->table('region')->insert($data);
                }
        }
}