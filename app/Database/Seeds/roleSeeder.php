<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class roleSeeder extends Seeder
{
        public function run()
        {
                $dataArray = [
                    ['type'    => 'admin'],
                    ['type'    => 'user']
                ];

                foreach ($dataArray as $data) {
                    // Using Query Builder
                    $this->db->table('role')->insert($data);
                }
        }
}