<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class centralSeeder extends Seeder
{
        public function run()
        {
            $this->call('regionSeeder');
            $this->call('categoriesSeeder');
            $this->call('roleSeeder');
        }
}