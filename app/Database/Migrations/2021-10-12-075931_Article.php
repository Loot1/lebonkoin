<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Article extends Migration
{
    public function up()
    {
        $this->forge->addField([
                'id'          => [
                        'type'           => 'INT',
                        'constraint'     => 5,
                        'auto_increment' => true,
                ],
                'title'       => [
                        'type'       => 'VARCHAR',
                        'constraint' => '50',
                ],
                'desc' => [
                        'type' => 'TEXT',
                ],
                'price'       => [
                        'type'       => 'INT',
                        'constraint' => '8',
                ],
                'extension' => [
                        'type' => 'VARCHAR',
                        'constraint' => '5',
                        'null' => true,
                ],
                'id_categorie' => [
                        'type' => 'INT',
                        'constraint' => '2',
                ],
                'id_user' => [
                        'type' => 'INT',
                        'constraint' => '2',
                ],
                'create_at' => [
                        'type' => 'DATETIME'
                ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('id_categorie', 'categories', 'id');
        $this->forge->addForeignKey('id_user', 'user', 'id');
        $this->forge->createTable('article');
    }

    public function down()
    {
        $this->forge->dropTable('article');
    }
}
