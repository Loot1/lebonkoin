<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Region extends Migration
{
     public function up()
    {
        $this->forge->addField([
                'id'          => [
                        'type'           => 'INT',
                        'constraint'     => 2,
                        'auto_increment' => true,
                ],
                'name'       => [
                        'type'       => 'VARCHAR',
                        'constraint' => '30',
                ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('region');
    }

    public function down()
    {
        $this->forge->dropTable('region');
    }
}
