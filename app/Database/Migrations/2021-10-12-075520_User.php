<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class User extends Migration
{
    public function up()
    {
        $this->forge->addField([
                'id'          => [
                        'type'           => 'INT',
                        'constraint'     => 5,
                        'auto_increment' => true,
                ],
                'mail'       => [
                        'type'       => 'VARCHAR',
                        'constraint' => '70',
                ],
                'password' => [
                        'type' => 'VARCHAR',
                        'constraint' => '70',
                ],
                'pseudo' => [
                        'type' => 'VARCHAR',
                        'constraint' => '25',
                ],
                'tel' => [
                        'type' => 'VARCHAR',
                        'constraint' => '10',
                        'null' => true,
                ],
                'checked' => [
                        'type' => 'BOOL',
                ],
                'token' => [
                        'type' => 'VARCHAR',
                        'constraint' => '70',
                        'null' => true,
                ],
                'code' => [
                        'type' => 'VARCHAR',
                        'constraint' => '6',
                        'null' => true,
                ],
                'id_role' => [
                        'type' => 'INT',
                        'constraint' => '2',
                ],
                'id_region' => [
                        'type' => 'INT',
                        'constraint' => '2',
                        'null' => true,
                ],
                'create_at' => [
                        'type' => 'DATETIME'
                ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('id_role', 'role', 'id');
        $this->forge->addForeignKey('id_region', 'region', 'id');
        $this->forge->createTable('user');
    }

    public function down()
    {
        $this->forge->dropTable('user');
    }
}
