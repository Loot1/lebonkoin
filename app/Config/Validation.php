<?php

namespace Config;

use CodeIgniter\Validation\CreditCardRules;
use CodeIgniter\Validation\FileRules;
use CodeIgniter\Validation\FormatRules;
use CodeIgniter\Validation\Rules;

class Validation
{
    //--------------------------------------------------------------------
    // Setup
    //--------------------------------------------------------------------

    /**
     * Stores the classes that contain the
     * rules that are available.
     *
     * @var string[]
     */
    public $ruleSets = [
        Rules::class,
        FormatRules::class,
        FileRules::class,
        CreditCardRules::class,
    ];

    /**
     * Specifies the views that are used to display the
     * errors.
     *
     * @var array<string, string>
     */
    public $templates = [
        'list'   => 'CodeIgniter\Validation\Views\list',
        'single' => 'CodeIgniter\Validation\Views\single',
    ];

    //--------------------------------------------------------------------
    // Rules
    //--------------------------------------------------------------------

    public $createArticle = [
        'title' => [
            'rules'  => 'required|regex_match[/^[a-zA-Z0-9àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ\'()& -!?,.;#$+-=:]*$/]|min_length[2]|max_length[100]',
            'errors' => [
                'required' => 'Vous devez renseigner le titre de l\'annonce.',
                'regex_match' => 'Le titre de l\'annonce ne peut contenir que des caractères alphanumérique ainsi que cette sélection: ~!#$%&*-_+=|:. .',
                'min_length[2]' => 'La longueur du titre de l\'annonce doit être supérieur à 2 caractères.',
                'max_length[100]' => 'La longueur du titre de l\'annonce doit être inférieur à 100 caractères.'
            ],
        ],
        'desc' => [
            'rules'  => 'required|regex_match[/^[a-zA-Z0-9àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ\'()& -!?,.;#$+-=:]*$/]|min_length[25]|max_length[1000]',
            'errors' => [
                'required' => 'Vous devez renseigner une description.',
                'regex_match' => 'La description ne peut contenir que des caractères alphanumérique ainsi que cette sélection: ~!#$%&*-_+=|:. .',
                'min_length' => 'La longueur du description doit être supérieur à 25 caractères.',
                'max_length' => 'La longueur du description doit être inférieur à 1000 caractères.'
            ],
        ],
        'price' => [
            'rules'  => 'required|integer|max_length[8]',
            'errors' => [
                'required' => 'Vous devez renseigner le prix.',
                'integer' => 'Le prix doit-être sous la forme d\'un entier.',
                'max_length' => 'La longueur du prix ne peux excéder 8 chiffres.'
            ],
        ],
        'id_categorie' => [
            'rules' => 'integer|max_length[1]',
            'errors' => [
                'integer' => 'La valeur de la catégorie doit-être sous la forme d\'un entier.',
                'max_length[1]' => 'La valeur attendue ne peut dépasser un chiffre.'
            ],
        ],
        'thumbnail' => [
            'rules' => 'ext_in[thumbnail,png,jpg,jpeg]|max_size[thumbnail,2000]|max_dims[thumbnail,2000,2000]|is_image[thumbnail]',
            'errors' => [
                'max_size' => 'Le poids de l\'image ne peut pas dépasser 2Mb.',
                'max_dims' => 'Les dimensions de l\'image ne peuvent excéder 2000x2000 pixels.',
                'ext_in' => 'L\'extension de l\'image doit-être jpg, jpeg ou png.',
                'is_image' => 'Vous devez transférer une image.'
            ],
        ],
        'checkbox' => [
            'rules' => 'required|integer|max_length[1]',
            'errors' => [
                'required' => 'Vous approuver les CGUs.',
                'integer' => 'La valeur attendu doit-être sous la forme d\'un entier.',
                'max_length' => 'La valeur attendue ne peut dépasser un chiffre.'
            ],
        ]
    ];

    public $signUp = [
        'username' => [
            'rules'  => 'required|alpha_numeric|min_length[2]|max_length[50]',
            'errors' => [
                'required' => 'Vous devez renseigner un nom utilisateur.',
                'alpha_numeric' => 'Le nom d\'utilisateur ne peut contenir que des caractères alphanumérique.',
                'min_length' => 'La longueur du nom d\'utilisateur doit être supérieur à 2 caractères.',
                'max_length' => 'La longueur du nom d\'utilisateur doit être inférieur à 50 caractères.'
            ],
        ],
        'email' => [
            'rules'  => 'required|valid_email|min_length[5]|max_length[50]',
            'errors' => [
                'required' => 'Vous devez renseigner un mail.',
                'valid_email' => 'Votre adresse mail ne peut contenir que des caractères alphanumérique.',
                'min_length' => 'La longueur de votre email doit être supérieur à 5 caractères.',
                'max_length' => 'La longueur de votre email doit être inférieur à 50 caractères.'
            ],
        ],
        'password' => [
            'rules'  => 'required|alpha_numeric_punct|min_length[2]|max_length[50]',
            'errors' => [
                'required' => 'Vous devez renseigner un mot de passe.',
                'alpha_numeric_punct' => 'Le mot de passe ne peut contenir que des caractères alphanumérique.',
                'max_length' => 'La longueur du mot de passe ne peux excéder 60 caractères.'
            ],
        ],
    ];

    public $signIn = [
        'email' => [
            'rules'  => 'required|valid_email|min_length[5]|max_length[50]',
            'errors' => [
                'required' => 'Vous devez renseigner un mail.',
                'valid_email' => 'Votre adresse mail ne peut contenir que des caractères alphanumérique.',
                'min_length' => 'La longueur de votre email doit être supérieur à 5 caractères.',
                'max_length' => 'La longueur de votre email doit être inférieur à 50 caractères.'
            ],
        ],
        'password' => [
            'rules'  => 'required|alpha_numeric_punct|min_length[2]|max_length[50]',
            'errors' => [
                'required' => 'Vous devez renseigner un mot de passe.',
                'alpha_numeric_punct' => 'Le mot de passe ne peut contenir que des caractères alphanumérique.',
                'max_length' => 'La longueur du mot de passe ne peux excéder 60 caractères.'
            ],
        ],
    ];

    public $formForgotPassword = [
        'email' => [
            'rules'  => 'required|valid_email|min_length[5]|max_length[50]',
            'errors' => [
                'required' => 'Vous devez renseigner un mail.',
                'valid_email' => 'Votre adresse mail ne peut contenir que des caractères alphanumérique.',
                'min_length' => 'La longueur de votre email doit être supérieur à 5 caractères.',
                'max_length' => 'La longueur de votre email doit être inférieur à 50 caractères.'
            ],
        ],
    ];

    public $verifyCode = [
        'code' => [
            'rules'  => 'required|alpha_numeric|max_length[6]',
            'errors' => [
                'required' => 'Vous devez renseigner un chiffre/nombre .',
                'alpha_numeric' => 'Le code ne peut contenir que des caractères alphanumérique.',
                'max_length' => 'La longeur maximal est de 1.'
            ],
        ],

    ];

    public $changePassword = [
        'password' => [
            'rules'  => 'required|alpha_numeric_punct|min_length[2]|max_length[50]',
            'errors' => [
                'required' => 'Vous devez renseigner un mot de passe.',
                'alpha_numeric_punct' => 'Le mot de passe ne peut contenir que des caractères alphanumérique.',
                'max_length' => 'La longueur du mot de passe ne peux excéder 60 caractères.'
            ],
        ],
        'passwordConfirm' => [
            'rules'  => 'required|alpha_numeric_punct|min_length[2]|max_length[50]',
            'errors' => [
                'required' => 'Vous devez renseigner un mot de passe.',
                'alpha_numeric_punct' => 'Le mot de passe ne peut contenir que des caractères alphanumérique.',
                'max_length' => 'La longueur du mot de passe ne peux excéder 60 caractères.'
            ],
        ],
    ];
}
