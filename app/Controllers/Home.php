<?php

namespace App\Controllers;

use App\Models\Article;
use App\Models\Categories;

class Home extends BaseController
{
    public function index()
    {
        helper('text');
        $session = session();
        
        $article = new Article();
        $result['randomTop'] = $article->getTop();
        // Récupération de toutes les catégories pour les afficher dynamiquement:
        $categories = new Categories();
        $result['categories'] = $categories->getAll();

        echo view('templates/header');
        echo view('pages/home', $result);
        echo view('templates/footer');
    }

    public function cgucgv()
    {
        $session = session();

        echo view('templates/header');
        echo view('pages/cgu-cgv');
        echo view('templates/footer');
    }

    public function mentions()
    {
        $session = session();

        echo view('templates/header');
        echo view('pages/mentions');
        echo view('templates/footer');
    }

    public function rgpd()
    {
        $session = session();

        echo view('templates/header');
        echo view('pages/rgpd');
        echo view('templates/footer');
    }

    public function test()
    {
        $session = session();

        echo view('templates/header');
        var_dump($_SESSION);
        echo view('templates/footer');
    }

    public function contactForm()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $mail = $_POST["email"];
            $message = $_POST["message"];
            if ($mail == "" || $message == "") {
                echo json_encode("formNotOk");
                die;
            }
            $email = \Config\Services::email();
            $email->setFrom('contact@lebonkoin.fr', 'Lebonkoin');
            $email->setTo($mail);
            $email->setSubject('Suite à votre message sur notre site');
            $email->setMessage('Contenu de votre message : '.$message);
            $email->send();
            echo json_encode("contactFormOk");
            die;
        }
    }
}
