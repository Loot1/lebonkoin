<?php

namespace App\Controllers;

use App\Models\Article;
use App\Models\Region;
use App\Models\User;

class UserController extends BaseController
{

    // Function allowing to register a user
    public function signUp()
    {

        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $validation = \Config\Services::validation();
            $validation->setRuleGroup('signUp');

            $data = [
                "username" => $_POST["username"],
                "email" => $_POST["email"],
                "password" => $_POST["password"]
            ];

            $validation->run($data, "signUp");

            $password = $data["password"];
            $pseudo = $data["username"];
            $email = $data["email"];

            $hash_password = password_hash($password, PASSWORD_DEFAULT);
            $date_create = date("Y-m-d");

            // Creation of a random key for the activation of the account.
            $set = "123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $key = substr(str_shuffle($set), 30, 45);

            $code = substr(str_shuffle($set), 1, 6);

            $user = array(
                "id_role"   => "2",
                "pseudo"    => $pseudo,
                "mail"      => $email,
                "password"  => $hash_password,
                "create_at" => $date_create,
                "token"     => $key,
                "code"      => $code,
            );

            $userModel = new User();

            if ($email == "" || $pseudo == "" || $password == "") {
                echo json_encode("formNotOk");
                die;
            } else {
                if ($userModel->verifyIfEmailExist($email) || $email == "") {
                    echo json_encode("emailNotOk");
                    die;
                } else {
                    if ($userModel->verifyPseudoExist($pseudo) || $pseudo == "") {
                        echo json_encode("pseudoNotOk");
                        die;
                    } else {
                        echo json_encode("signUpOk");
                        $userModel->signUp($user);

                        $email = \Config\Services::email();

                        $message = "
                        <html>
                            <h1>Confirmation d'inscription</h1>
                                <p>bonjour $pseudo</p>         
                                <p>Pour valider votre inscription veuillez cliquer sur le lien ci dessous et insérer le code/</p>   
                                <span> " . $code . " </span>      
                                <h4><a href='" . "http://lebonkoin.localhost/UserController/verifyAccount/" . $key . "'>cliquez ici</a>.</h4>         
                            </html>";

                        $email->setFrom('lebonkoin@gmail.com', 'Lebonkoin');
                        $email->setTo("test@exemple.com");
                        $email->setBCC('them@their-example.com');

                        $email->setSubject('Confirmation inscription');
                        $email->setMessage($message);

                        $email->send();
                        die;
                    }
                }
            }
        }
    }

    // Function allowing to log in a user
    public function signIn()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            $userModel = new User();
            $validation = \Config\Services::validation();
            $validation->setRuleGroup('signUp');

            $data = [
                "email" => $_POST["email"],
                "password" => $_POST["password"]
            ];

            $validation->run($data, "signIn");

            $email = $data["email"];
            $password = $data["password"];

            $user = $userModel->signIn($email);

            if ($user) {
                if ($user->checked != "0") {
                    $passwordHash = $user->password;
                    if (password_verify($password, $passwordHash)) {
                        $session = session();

                        $data = array(
                            "id"        => $user->id,
                            "email"     => $user->mail,
                            "pseudo"    => $user->pseudo,
                            "role"    => $user->id_role
                        );

                        $_SESSION['user'] = $data;
                        echo json_encode("signInOk");
                        die;
                    }
                } else {
                    echo json_encode("accountNoVerify");
                    die;
                }
            } else {
                echo json_encode("mailNoExist");
                die;
            }
        }
    }

    // Function allowing to log out user
    public function logOut()
    {
        $session = session();

        unset($_SESSION['_ci_previous_url']);
        unset($_SESSION['__ci_last_regenerate']);

        $session->destroy();
        header('Location: /accueil');
        exit;
    }

    // Function allowing to forgot password user
    public function forgotPassword()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $userModel = new User();
            $validation = \Config\Services::validation();
            $validation->setRuleGroup('signUp');

            $data = [
                "email" => $_POST["email"],
            ];

            $validation->run($data, "formForgotPassword");

            $mail = $data["email"];

            $user = $userModel->select("user", "mail", $mail);
            if ($user) {
                // Creation of a random key for the activation of the account.
                $set = "123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $code = substr(str_shuffle($set), 1, 6);

                $data = array(
                    "code" => $code
                );

                if ($userModel->modify("user", "mail", $mail, $data)) {
                    $email = \Config\Services::email();

                    $message = "
                        <html>
                            <h1>Changement de mot de passe</h1>
                                <p>Bonjour $user->pseudo</p>         
                                <p>Pour changer votre mot de passe merci de cliquer sur le lien et de saisir le code</p>   
                                <span> " . $code . " </span>      
                                <h4><a href='" . "http://lebonkoin.localhost/UserController/changePassword/" . $user->token . "'>cliquez ici</a>.</h4>         
                            </html>";

                    $email->setFrom('lebonkoin@gmail.com', 'Lebonkoin');
                    $email->setTo("test@exemple.com");
                    $email->setBCC('them@their-example.com');

                    $email->setSubject('Changement mot de passe');
                    $email->setMessage($message);

                    $email->send();
                    echo json_encode("emailOk");
                    die;
                }
            } else {
                echo json_encode("emailNotExist");
                die;
            }
        }

        header('Location: /accueil');
    }

    // Function allowing to verify account user
    public function verifyAccount()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $userModel = new User();

            $code = $_POST["code"];
            $token = $_POST["token"];
            $user = $userModel->verifyToken($token);

            $data = array(
                "checked" => "1",
                "code" => ""
            );

            if ($user->token == $token) {
                $userModel->modify("user", "code", $code, $data);
                echo json_encode("verifyAccountOk");
                die;
            } else {
                echo json_encode("verifyAccountNotOk");
                die;
            }
        };
        return view("pages/page-verify-account");
    }

    // Function allowing to verify code and token user
    public function verifyCode()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $userModel = new User();
            $validation = \Config\Services::validation();
            $validation->setRuleGroup('verifyCode');

            $data = [
                "code" => $_POST["code"],
            ];

            $validation->run($data, "verifyCode");

            $code = $data["code"];
            $token = $_POST["token"];

            $user = $userModel->select("user", "token", $token);

            if ($user->code == $code && $user->token == $token) {
                echo json_encode("verifyCodeOk");
                die;
            } else {
                echo json_encode("verifyCodeNotOk");
                die;
            }
        }
    }

    // Function allowing to change password after verify code user
    public function changePassword()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $userModel = new User();

            $validation = \Config\Services::validation();
            $validation->setRuleGroup('changePassword');

            $data = [
                "password" => $_POST["password"],
                "passwordConfirm" => $_POST["passwordConfirm"],
            ];

            $validation->run($data, "changePassword");

            $token = $_POST["token"];
            $password = $data["password"];
            $passwordConfirm = $data["passwordConfirm"];

            $passwordHash = password_hash($password, PASSWORD_DEFAULT);

            $data = array(
                "password" => $passwordHash
            );

            if ($password == $passwordConfirm) {
                if ($userModel->modify("user", "token", $token, $data)) {
                    echo json_encode("changePasswordOk");
                    die;
                } else {
                    echo json_encode("error");
                    die;
                }
            } else if ($password == "" && $passwordConfirm = "") {
                echo json_encode("changePasswordNotOk");
                die;
            } else {
                echo json_encode("changePasswordNotOk");
                die;
            }
        }
        return view("pages/page-verify-password-code");
    }

    // Function allowing to redirect profil user
    public function Profil()
    {
        $session = session();
        if(empty($_SESSION['user'])){
            header('Location: /accueil');
            exit;
        }
        helper('text');
        helper(['form', 'url']);

        $article = new Article();
        $user = new User();
        $region = new Region();
        $result['userArticle'] = $article->getAllByIdUser($_SESSION['user']['id']);
        $result['user'] = $user->getOne($_SESSION['user']['id']);
        if (!empty($result['user']->id_region)) {
            $result['user']->region = $region->getOne($result['user']->id_region)->name;
        }
        $result['regions'] = $region->getAll();
        echo view('templates/header');
        echo view('user/backUser', $result);
        echo view('templates/footer');
    }

    // Function allowing to modify profil user
    public function modify($idUser)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_SESSION['user']['id'] == $idUser) {
            $user = new User();
            if(empty($_POST['tel'])){
                $tel = NULL;
            }else{
                $tel = $_POST['tel'];
            }

            if(!empty($_POST['region'])){
                $region = $_POST['region'];
            }else{
                $region = NULL;
            }
            if(!empty($tel) && !empty($_POST['region'])){
                $checked = 2;
            } else {
                $checked = 1;
            }
            
            $data = [
                'id_region' => $region,
                'tel' => $tel,
                'checked' => $checked,
            ];
            $result['code'] = $user->modify("user", "id", $idUser, $data);
        }

        header('Location: /profil');
        exit;
    }

    public function Admin()
    {
        $session = session();
        if($_SESSION['user']['role'] !== '1'){
            header('Location: /accueil');
            exit;
        }
		// Get data 
		$users = new User();
        $article = new Article();

        $result['users'] = $users->getAll();
        $result['articles'] = $article->getAll();
        echo view('templates/header');
        echo view('user/backAdmin', $result);
        echo view('templates/footer');
	}

    public function delete($id)
    {
		// Get data 
		$users = new User();
        $result['users'] = $users->deleteUser($id);

        header('Location: /admin');
        exit;
	}

}
