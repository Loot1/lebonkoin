<?php

namespace App\Controllers;
use App\Models\Article;
use App\Models\Categories;
use App\Models\Region;

class ArticleController extends BaseController
{
    public function create()
    {
        $session = session();
        if(empty($_SESSION)){
            header('Location: /accueil');
            die;
        }
        // Setup:
        helper(['form', 'url']);
        $validation = \Config\Services::validation();
        $isValid = 0;

        // Traitement:
        if (!empty($_POST)) {
            $thumbnail = $this->request->getFile('thumbnail');
            $thumbnailExtension = '.'.$thumbnail->getClientExtension();

            $dataToCheck = [
                'title' => $_POST['title'],
                'desc' => $_POST['desc'],
                'price' => $_POST['price'],
                'id_categorie' => $_POST['category'],
                'thumbnail' => $thumbnail,
                'checkbox' => $_POST['checkbox']
            ];

            $validation->setRuleGroup('createArticle');
            if ($validation->run($dataToCheck, 'createArticle')) {
                // Traitement final:
                $isValid = 1;
                $dataToSend = [
                    'title' => $_POST['title'],
                    'desc' => $_POST['desc'],
                    'price' => $_POST['price'],
                    'extension' => $thumbnailExtension,
                    'id_categorie' => $_POST['category'],
                    'id_user' => $_SESSION['user']['id']
                ];

                // Appel de la méthode qui insert et recupère le lastInsertId:
                $article = new Article(); 
                $idArticle = $article->create($dataToSend);

                // Upload de l'image:
                $thumbnailToUpload = \Config\Services::image()
                    ->withFile($thumbnail->getTempName())
                    ->fit(600,300,'center')
                    ->save(getcwd().'/uploads/'.$idArticle.$thumbnailExtension, 70);
            }
        }

        // Affichage:
        echo view('templates/header');

        if ($isValid) {
            echo view('articles/create/success');
        } else {
            // Récupération de toutes les catégories pour les afficher dynamiquement dans le form:
            $categories = new Categories();
            $categories = $categories->getAll();

            // Inclusion des REGEX pour la sécu Front:
            require_once dirname(__FILE__).'/../../public/utils/regex.php';

            echo view('articles/create/form', [
                'validation' => $validation,
                'categories' => $categories
            ]);
        }

        echo view('templates/footer');
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function getSingle($id)
    {
        $session = session();

        $article = new Article();
        $result['article'] = $article->getSingle($id);

        $pic = dirname(__FILE__)."/../../public/uploads/".$result['article']->id.$result['article']->extension;
        if(!file_exists($pic)){
            $pic = base_url("public/uploads/default.jpg");
        }else{
            $pic = base_url("public/uploads/".$result['article']->id.$result['article']->extension);
        }
        $result['pic'] = $pic;

        echo view('templates/header');
        echo view('articles/single', $result);
        echo view('templates/footer');
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function modify($idArticle)
    {
        $session = session();

        helper('text');
        helper(['form', 'url']);
        $validation = \Config\Services::validation();

        $articles = new Article();
        $categories = new Categories();
        $result['categories'] = $categories->getAll();
        $result['code'] = "";
        
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if (!empty($_POST)) {
                $thumbnail = $this->request->getFile('thumbnail');
                if(!empty($thumbnail->getName())){
                    $thumbnailExtension = '.'.$thumbnail->getClientExtension();
                    $dataToCheck = [
                        'title' => $_POST['title'],
                        'desc' => $_POST['desc'],
                        'price' => $_POST['price'],
                        'id_categorie' => $_POST['category'],
                        'thumbnail' => $thumbnail,
                        'checkbox' => $_POST['checkbox']
                    ];
                }else{
                    $dataToCheck = [
                        'title' => $_POST['title'],
                        'desc' => $_POST['desc'],
                        'price' => $_POST['price'],
                        'id_categorie' => $_POST['category'],
                        'checkbox' => $_POST['checkbox']
                    ];
                }
                
                $validation->setRuleGroup('createArticle');
                if ($validation->run($dataToCheck, 'createArticle')) {
                    // Traitement final:
                    if(!empty($thumbnail)){
                        $dataToSend = [
                            'title' => $_POST['title'],
                            'desc' => $_POST['desc'],
                            'price' => $_POST['price'],
                            'extension' => $thumbnailExtension,
                            'id_categorie' => $_POST['category'],
                        ];
                        // Upload de l'image:
                        $thumbnailToUpload = \Config\Services::image()
                            ->withFile($thumbnail->getTempName())
                            ->fit(600,300,'center')
                            ->save(getcwd().'/uploads/'.$idArticle.$thumbnailExtension, 70);
                    }else{
                        $dataToSend = [
                            'title' => $_POST['title'],
                            'desc' => $_POST['desc'],
                            'price' => $_POST['price'],
                            'id_categorie' => $_POST['category'],
                        ];
                    }
                    
                    // Appel de la méthode qui Update
                    $article = new Article(); 
                    $result['code'] = $article->modify($idArticle, $dataToSend);
                    
                }
            }
            
        }

        $result['article'] = $articles->getSingle($idArticle);
        require_once dirname(__FILE__).'/../../public/utils/regex.php';

        echo view('templates/header');
        echo view('articles/modify', $result);
        echo view('templates/footer');
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function deleteArticle($idArticle)
    {
        helper('text');

        $articles = new Article();
        $result['code'] = $articles->deleteArticle($idArticle);

        header('Location: /UserController/Profil');
        exit;
    }

        ////////////////////////////////////////////////////////////////////////////////

    public function deleteArticleAdmin($idArticle)
    {
        helper('text');

        $articles = new Article();
        $result['code'] = $articles->deleteArticle($idArticle);

        header('Location: /users');
        exit;
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function getAllByCategory($idCategory)
    {
        $session = session();

        // Récupération de la liste des catégories et régions pour le filtre de recherche:
        $categories = new Categories();
        $result['categories'] = $categories->getAll();
        $regions = new Region();
        $result['regions'] = $regions->getAll();

        helper('text');

        $articles = new Article();
        $result['articles'] = $articles->getAllByCategory($idCategory);

        echo view('templates/header');
        echo view('articles/list', $result);
        echo view('templates/footer');
    }

    ////////////////////////////////////////////////////////////////////////////////

    public function getAllBySearch($idCategory, $articleName, $minPrice, $maxPrice, $idRegion)
    {
        $articles = new Article();
        $result['articles'] = $articles->getAllBySearch($idCategory, $articleName, $minPrice, $maxPrice, $idRegion);

        echo json_encode($result['articles']);
        die;
    }
}