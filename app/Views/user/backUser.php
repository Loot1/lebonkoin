<div class="row h-100 m-5 flex-column align-items-center">
    <div class="col-10 col-md-8 col-lg-6 bg-cyan pb-3 pt-3 rounded-2 d-flex flex-column align-items-center text-white mb-5">
        <h3 class="text-center text-white">Votre profil</h3>
        <p><strong>Pseudo : </strong><?= $user->pseudo ?></p>
        <p><strong>Mail : </strong><?= $user->mail ?></p>
        <p><strong>Téléphone : </strong><?= $user->tel ?? 'Non renseigné' ?></p>
        <p><strong>Region : </strong><span id="region"><?= $user->region ?? 'Non renseigné' ?></span></p>
        <p><strong>Utilisateur vérifié : </strong><?= $Check = $user->checked == 2 ? 'Oui' : 'Non' ?></p>
        <button class="btn btn-sm mb-3 btn-warning " id="modify">Modifier <i class="fas fa-sm fa-pen"></i></button>
        <div id="FormRegion" class="d-none bg-info rounded w-50 p-2 text-center">

            <?= form_open('http://lebonkoin.localhost/UserController/modify/'.$user->id) ?>
                <div>
                    <label for="tel" class="form-label">Téléphone</label>
                    <input class="form-control" type="tel" name="tel" id="tel" value="<?= $user->tel ?? '' ?>">
                </div>
                <div class="mb-3">
                    <label for="region" class="form-label">Region:</label>
                    <select class="form-select" id="region" name="region">
                        <option value="" <?= $select = empty($user->id_region) ? 'selected' : ''; ?>>Sélectionner une Region</option>
                        <?php foreach ($regions as $region): ?>
                            <option value="<?= $region->id ?>" <?= $select = !empty($user->id_region) && $user->id_region == $region->id ? 'selected' : ''; ?>><?= $region->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <button type="submit" class="btn btn-success">Modifier <i class="fas fa-pen"></i></button>
            </form>
        </div>
    </div>
    <div class="separator w-25"></div>

    <div class="col-12">
        <h3 class="text-center">Liste de vos annonces</h3>
        <div class="d-flex flex-column align-items-center pt-2">
            <?php foreach($userArticle as $item): 
                $pic = dirname(__FILE__)."/../../../public/uploads/".$item->id.$item->extension;
                if(!file_exists($pic)){
                    $pic = base_url("public/uploads/default.jpg");
                }else{
                    $pic = base_url("public/uploads/".$item->id.$item->extension);
                }
            ?>
            <div class="card mb-3" style="width: 90%;">
                <div class="row g-0">
                    <div class="col-md-4">
                        <img src="<?= $pic ?>" class="img-fluid rounded-start" alt="Illustration de l'annonce <?= htmlentities($item->title) ?>">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= htmlentities($item->title) ?></h5>
                            <p class="card-text"><?= word_limiter(htmlentities($item->desc), 20) ?></p>
                            <p class="card-text"><small class="text-muted">Ajouté le <?= date("d/m/Y", strtotime(htmlentities($item->create_at))) ?></small></p>
                            <div class="d-flex flex-column flex-md-row">
                                <p class="me-2"><?= htmlentities($item->price) ?> €</p>
                                <a href="/article-<?= htmlentities($item->id) ?>" class="btn btn-primary ms-2">Voir</a>
                                <a href="/modifier/article-<?= htmlentities($item->id) ?>" class="btn btn-warning ms-2">Modifier</a>
                                <a href="/supprimer/article-<?= htmlentities($item->id) ?>" class="btn btn-danger ms-2">Supprimer</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<script src="<?= base_url("public/js/profil.js") ?>"></script>
