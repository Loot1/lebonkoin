<div class="row h-100 m-5 flex-column align-items-center">
    <h1 class="text-center">Panel Administrateur</h1>
    <div class="col-10">
        <div class='container' style='margin-top: 20px;'>
            <h3 class="text-center">Liste des utilisateurs</h3>
            <table id="users" class="display" style="width:100%">
                <thead class="text-center">
                    <tr class='border-1'>
                        <th class='border-1'>ID</th>
                        <th class='border-1'>Name</th>
                        <th class='border-1'>Email</th>
                        <th class='border-1'>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    foreach ($users as $user) {
                        echo "<tr class='border-1'>";
                            echo "<td class='border-1'>" . $user->id . "</td>";
                            echo "<td class='border-1'>" . $user->pseudo . "</td>";
                            echo "<td class='border-1'>" . $user->mail . "</td>";
                            echo "<td class='border-1 text-center'><a class='btn btn-danger' href='/supprimer/utilisateur-".$user->id."'><i class='fas fa-user-minus'></i></a></td>";
                        echo "</tr>";
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="separator w-25 mt-5 mb-5"></div>
    <div class="col-10">
        <div class='container' style='margin-top: 20px;'>
            <h3 class="text-center">Liste des annonces</h3>
            <table id="articles" class="display" style="width:100%">
                <thead class="text-center">
                    <tr class='border-1'>
                        <th class='border-1'>ID</th>
                        <th class='border-1'>Titre</th>
                        <th class='border-1'>Description</th>
                        <th class='border-1'>Prix</th>
                        <th class='border-1'>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    foreach ($articles as $article) {
                        echo "<tr class='border-1'>";
                            echo "<td class='border-1'>" . $article->id . "</td>";
                            echo "<td class='border-1'>" . $article->title . "</td>";
                            echo "<td class='border-1'>" . $article->desc . "</td>";
                            echo "<td class='border-1'>" . $article->price . "</td>";
                            echo "<td class='border-1 text-center'><a class='btn btn-danger' href='/supprimer/article-".$article->id."'><i class='fas fa-times'></i></a></td>";
                        echo "</tr>";
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
