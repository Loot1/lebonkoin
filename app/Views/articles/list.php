<div class="row flex-column flex-md-row align-items-center justify-content-center mx-5 my-3 p-5 filtre">
    <div class="d-flex flex-column col-12 col-md-2 mb-2">
        <label for="category" class="mb-2">Catégorie: </label>
        <select name="category" id="category" class="form-select">
            <option selected></option>
            <?php foreach ($categories as $category) : ?>
                <option value="<?= $category->id ?>"><?= $category->type ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-12 col-md-2 d-md-block mb-2">
        <label for="articleName" class="mb-2">Nom de l'annonce: </label>
        <input type="text" name="articleName" id="articleName" class="form-control">
    </div>
    <div class="col-12 col-md-2 mb-2">
        <label for="minPrice" class="mb-2">Prix min: </label>
        <input type="number" name="minPrice" id="minPrice" min="1" class="form-control">
    </div>
    <div class="col-12 col-md-2 mb-2">
        <label for="maxPrice" class="mb-2">Prix max: </label>
        <input type="number" name="maxPrice" id="maxPrice" min="1" class="form-control">
    </div>
    <div class="col-12 col-md-2 mb-2">
        <label for="region" class="mb-2">Région: </label>
        <select name="region" id="region" class="form-select">
            <option selected></option>
            <?php foreach ($regions as $region) : ?>
                <option value="<?= $region->id ?>"><?= $region->name ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<div id="articlesList" class="d-flex flex-column align-items-center pt-2">
    <?php foreach ($articles as $item) :
        $pic = dirname(__FILE__) . "/../../../public/uploads/" . $item->id . $item->extension;
        if (!file_exists($pic)) {
            $pic = base_url("public/uploads/default.jpg");
        } else {
            $pic = base_url("public/uploads/" . $item->id . $item->extension);
        } ?>

        <div class="card mb-3" style="width: 90%;">
            <div class="row g-0">
                <div class="col-md-4">
                    <img src="<?= $pic ?>" class="img-fluid rounded-start" alt="Illustration de l'annonce <?= htmlentities($item->title) ?>">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title"><?= htmlentities($item->title) ?></h5>
                        <p class="card-text mb-0"><?= word_limiter(htmlentities($item->desc), 75) ?></p>
                        <p class="card-text"><small class="text-muted">Ajouté le <?= date("d/m/Y", strtotime(htmlentities($item->create_at))) ?></small></p>
                        <div class="d-flex justify-content-end">
                            <div class="col d-flex justify-content-end pt-5">
                                <p class="me-2"><?= htmlentities($item->price) ?> €</p>
                                <a href="/article-<?= htmlentities($item->id) ?>" class="btn btn-primary ms-2">Voir</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<script src="<?= base_url('/public/js/search.js') ?>"></script>