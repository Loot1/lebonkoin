<div class="container my-3 text-center p-5" style="border: solid 1px #00838d; border-radius: 5rem">
    <img class="img-fluid" src="<?= $pic ?>" alt="Illustration de l'annonce <?= htmlentities($article->title) ?>">
    <h2><?= htmlentities($article->title) ?></h2>
    <div><?= htmlentities($article->price) ?> €</div>
    <div><?= date("d/m/Y", strtotime(htmlentities($article->create_at))) ?></div>
    <h4>Description:</h4>
    <p><?= htmlentities($article->desc) ?></p>
    <h4>Informations vendeur:</h4>
    <ul class="list-group list-group-flush">
        <li class="list-group-item">Pseudo: <?= htmlentities($article->pseudo) ?> <?= htmlentities($article->checked) == 2 ? '<em>Vérifié</em>' : '' ?></li>
        <li class="list-group-item">Email: <a href="mailto:<?= htmlentities($article->mail) ?>"><?= htmlentities($article->mail) ?></a></li>
        <li class="list-group-item">Téléphone: <a href="tel:<?= htmlentities($article->tel) ?>"><?= htmlentities($article->tel) ?></a></li>
        <li class="list-group-item">Région: <?= htmlentities($article->name) ?></li>
    </ul>
</div>