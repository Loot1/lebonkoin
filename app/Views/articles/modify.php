
<h2 class="text-center mt-5">Modification de votre annonce:</h2>
<?php if($code == true) {?>
    <div class="text-center h5 pt-2 pb-2 alert-success">
        Bravo votre annonce a été modifié !
    </div>
<?php }elseif($code != ""){ ?>
    <div class="text-center h5 pt-2 pb-2 alert-warning">
        Aucun changement n'a été effectué
    </div>
<?php } ?>
<div class="row justify-content-center">
    <div class="col-6 mb-5">
        <?= form_open('http://lebonkoin.localhost/ArticleController/modify/'.$article->id, 'enctype="multipart/form-data"') ?>
            <div class="mb-3">
                <label for="title" class="form-label">Intitulé:</label>
                <input required pattern="<?=REGALPHANUMSPECHARS?>" title="N'utilisez que des lettres, des chiffres et les caractères suivants: ~!#$%&*-_+=|:. ." minlength="2" maxlength="100" type="text" class="form-control" id="title" name="title" value="<?= $article->title ?? '' ?>">
            </div>
            <div class="mb-3">
                <label for="desc" class="form-label">Description:</label>
                <textarea class="form-control" id="desc" name="desc" rows="5"><?= $article->desc ?? '' ?></textarea>
            </div>
            <div class="mb-3">
                <label for="price" class="form-label">Prix:</label>
                <input required pattern="<?=REGNUM?>" title="N'utilisez que des chiffres." min="1" maxlength="12" type="number" class="form-control" id="price" name="price" value="<?= $article->price ?? '' ?>">
            </div>
            <div class="mb-3">
                <label for="category" class="form-label">Categorie:</label>
                <select required class="form-select" id="category" name="category">
                    <option selected>Sélectionner une catégorie</option>
                    <?php foreach ($categories as $category): ?>
                        <option value="<?= $category->id ?>" <?= $select = $article->id == $category->id ? 'selected' : ''; ?>><?= $category->type ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="mb-3">
                <label for="thumbnail" class="form-label">Image:</label>
                <input type="file" class="form-control" id="thumbnail" name="thumbnail">
                <div id="fileHelp" class="form-text">Poids max: 2Mo ; Dimensions min: 200 x 200 px ; Format accepté: jpeg/jpg/png.</div>
            </div>
            <div class="mb-3 form-check">
                <input required type="checkbox" class="form-check-input" name="checkbox" id="checkbox" value="1">
                <label class="form-check-label" for="checkbox">Je suis en accord avec les CGUs.</label>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary">Publier</button>
            </div>
        </form>
    </div>
</div>
