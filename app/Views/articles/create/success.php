<div class="d-flex flex-column align-items-center my-5">
    <h3>Votre annonce a bien été crée.</h3>
    <div>
        <a class="text-decoration-none" href="/creer">
            <button>Créer une nouvelle annonce</button>
        </a>
        <a class="text-decoration-none" href="/accueil">
            <button>Retour à la page d'accueil</button>
        </a>
    </div>
</div>
