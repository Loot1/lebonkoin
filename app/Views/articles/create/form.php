<section id="addArticle" class="d-flex flex-column align-items-center mb-5">
    <h2 class="mt-5 text-center">Ajouter une annonce:</h2>
    <div class="separator"></div>

    <?= (!empty($_POST)) ? $validation->listErrors() : '' ?>

    <?= form_open('http://lebonkoin.localhost/ArticleController/create', 'enctype="multipart/form-data"') ?>

    <div class="mb-3">
        <label for="title" class="form-label fw-bold">Intitulé:</label>
        <input required type="text" class="form-control" id="title" name="title" pattern="<?= REGALPHANUMSPECHARS ?>" title="N'utilisez que des lettres, des chiffres et les caractères suivants: ~!#$%&*-_+=|:. ." minlength="2" maxlength="100" value="<?= set_value('title') ?> ">
    </div>
    <div class="mb-3">
        <label for="desc" class="form-label fw-bold">Description:</label>
        <div><textarea name="desc" id="desc" class="form-control"><?= set_value('desc') ?></textarea></div>
    </div>
    <div class="mb-3">
        <label for="price" class="form-label fw-bold">Prix:</label>
        <input required type="number" class="form-control" id="price" name="price" pattern="<?= REGNUM ?>" title="N'utilisez que des chiffres." min="1" maxlength="12" value="<?= set_value('price') ?>">
    </div>
    <div class="mb-3">
        <label for="category" class="form-label fw-bold">Catégorie:</label>
        <select required class="form-select" id="category" name="category">
            <option selected>Sélectionner une catégorie</option>
            <?php foreach ($categories as $category) : ?>
                <option value="<?= $category->id ?>"><?= $category->type ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="mb-3">
        <label for="thumbnail" class="form-label fw-bold">Image:</label>
        <input type="file" class="form-control" id="thumbnail" name="thumbnail">
        <div id="fileHelp" class="form-text fst-italic">Poids max: 2Mo ; Dimensions min: 200 x 200 px ; Format accepté: jpeg/jpg/png.</div>
    </div>
    <div class="mb-3 form-check">
        <input required type="checkbox" class="form-check-input" id="checkbox" name="checkbox" value="1">
        <label class="form-check-label" for="checkbox">J'accepte les CGUs.</label>
    </div>
    <button type="submit" class="btn btn-primary">Publier</button>
    </form>
</section>