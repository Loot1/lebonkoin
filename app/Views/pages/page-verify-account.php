<!doctype html>
<html lang="fr" dir="ltr">

<head>
    <title>Lebonkoin</title>
    <meta name="language" content="fr-FR">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url("/public/css/verifyAccount.css") ?>">
</head>

<body>
    <div class="container">
        <div class="row">
            <div id="card-verify" class="card mt-5">
                <div class="row">
                    <div class="d-flex flex-column align-items-center">
                        <span id="title" class="text-white mt-5">Vérification de compte</span>
                        <div class="separator"></div>
                        <div id="message" class="alert alert-danger d-none" role="alert">
                        </div>
                    </div>
                </div>
                <div id="content" class="row">
                    <span id="indication" class="text-white mt-2 mb-4 d-flex justify-content-center">Merci de saisir le code à 6 lettres/chiffres reçu via mail.</span>

                    <form id="form-verify" action="">
                        <div class="col d-flex justify-content-center container-form">
                            <input type="text" class="form-control input-size" id="number-1" name="number-1" maxlength="1">
                            <input type="text" class="form-control input-size" id="number-2" name="number-2" maxlength="1">
                            <input type="text" class="form-control input-size" id="number-3" name="number-3" maxlength="1">
                            <input type="text" class="form-control input-size" id="number-4" name="number-4" maxlength="1">
                            <input type="text" class="form-control input-size" id="number-5" name="number-5" maxlength="1">
                            <input type="text" class="form-control input-size" id="number-6" name="number-6" maxlength="1">
                        </div>
                        <div class="row mt-5">
                            <div class="col d-flex justify-content-center">
                                <button type="button" id="btn-envoyer" class="btn btn-outline-light text-uppercase l-sp-5">Vérification</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

</body>

<footer class="fixed-bottom bg-cyan">
    <em class="text-white">&copy; 2021</em>
</footer>
<script src="https://kit.fontawesome.com/4013fbf04f.js" crossorigin="anonymous"></script>
<script src="<?= base_url("public/js/jQuery/jquery-3.6.0.min.js") ?>"></script>
<script src="<?= base_url("public/js/bootstrap/bootstrap.min.js") ?>"></script>


<script>
    $("document").ready(function() {

        var container = document.getElementsByClassName("container-form")[0];
        container.onkeyup = function(e) {
        
            var target = e.srcElement;
            var maxLength = parseInt(target.attributes["maxlength"].value, 5);
 
            var myLength = target.value.length;
            if (myLength >= maxLength) {
                var next = target;
                while (next = next.nextElementSibling) {
                    if (next == null)
                        break;
                    if (next.tagName.toLowerCase() == "input") {
                        next.focus();
                        break;
                    }
                }
            }
        }
        
        $("#btn-envoyer").on("click", function(e) {

            e.preventDefault();

            var form = document.getElementById("form-verify");
            var data = new FormData(form);
            var formData = new FormData();
            var code = "";

            $test = $("#content");

            for (var value of data.values()) {
                code += value
            }

            var url = $(location).attr('href');
            var segments = url.split('/');
            var token = segments[5];

            formData.append("code", code);
            formData.append("token", token)

            fetch("http://lebonkoin.localhost/index.php/UserController/verifyAccount", {
                method: "POST",
                body: formData,
                type: "json"
            }).then(response => response.json()).then(formData => {
                if (formData == "verifyAccountOk") {
                    $("#message").removeClass("d-none");
                    $("#message").removeClass("alert-danger");
                    $("#message").addClass("alert-success");
                    $("#message").text("Le compte es bien vérifié, vous allez être redirigé.")

                    var url = 'window.location.replace("http://lebonkoin.localhost/accueil");';
                    setTimeout(url, 3000);
                }
                if (formData == "verifyAccountNotOk") {
                    $("#message").removeClass("d-none");
                    $("#message").text("Une erreur es survenue merci de réessayer.")
                }
            }).catch((error) => {

            })
        });

        // TODO

    });
</script>

</html>