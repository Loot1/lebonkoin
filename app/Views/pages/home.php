<section id="categorie">
    <div class="container mt-5">
        <div class="d-flex flex-column align-items-center">
            <h1 id="titre-categorie">Toutes nos catégories</h1>
            <div class="separator"></div>
        </div>
        <div class="row justify-content-center">
            <?php foreach ($categories as $category) : ?>
                <div class="col-6 col-lg-3 ">
                    <a class="card1" href="/recherche/categorie-<?= $category->id ?>">
                        <h3><?= $category->type ?></h3>
                        <div class="go-corner">
                            <div class="go-arrow">
                                →
                            </div>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
    <path fill="#00838d" fill-opacity="0.5" d="M0,160L48,181.3C96,203,192,245,288,234.7C384,224,480,160,576,144C672,128,768,160,864,197.3C960,235,1056,277,1152,261.3C1248,245,1344,171,1392,133.3L1440,96L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
</svg>
<div class="row ">
    <h1 id="title-site" class="d-flex justify-content-center" style="position: absolute; z-index: index 1;">lebonkoin</h1>
</div>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
    <path fill="#00838d" fill-opacity="0.5" d="M0,160L48,181.3C96,203,192,245,288,234.7C384,224,480,160,576,144C672,128,768,160,864,197.3C960,235,1056,277,1152,261.3C1248,245,1344,171,1392,133.3L1440,96L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"></path>
</svg>



<section id="top-article" class="d-flex flex-column align-items-center mb-5">
    <div class="separator"></div>
    <h2 id="titre-top-article" class="mb-5">Top articles</h2>
    <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <?php
            $count = 0;
            foreach ($randomTop as $top) {
                $pic = dirname(__FILE__) . "/../../../public/uploads/" . $top->id . $top->extension;
                if (!file_exists($pic)) {
                    $pic = base_url("public/uploads/default.jpg");
                } else {
                    $pic = base_url("public/uploads/" . $top->id . $top->extension);
                }

            ?>
                <div class="carousel-item <?= $active = $count == 0 ? 'active' : '' ?>" data-bs-interval="5000">
                    <a href="/article-<?= htmlentities($top->id) ?>"><img src="<?= $pic ?>" class="d-block img-fluid" alt="..."></a>
                    <div class="carousel-caption d-none d-md-block bg-white text-black rounded-1" style="--bs-bg-opacity: .5;">
                        <h5><?= $top->title ?></h5>
                        <p><?= word_limiter($top->desc, 25) ?></p>
                    </div>
                </div>
            <?php
                $count++;
            }
            ?>
        </div>
    </div>
</section>