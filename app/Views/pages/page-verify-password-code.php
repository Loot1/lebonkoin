<!doctype html>
<html lang="fr" dir="ltr">

<head>
    <title>Lebonkoin</title>
    <meta name="language" content="fr-FR">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url("/public/css/verifyAccount.css") ?>">
</head>

<body>
    <div class="container">
        <div class="row">
            <div id="card-verify" class="card mt-5">
                <div class="row">
                    <div class="d-flex flex-column align-items-center">
                        <span id="title" class="text-white mt-5">Changement de mot de passe</span>
                        <div class="separator"></div>
                        <div id="message" class="alert alert-danger d-none" role="alert">
                        </div>
                    </div>
                </div>
                <div id="content" class="row">

                    <form id="form-change-password" class="d-none" action="" method="POST">
                        <div class="row justify-content-center">
                            <div class="col-4">
                                <input type="password" class="form-control mt-3" id="password" name="password" placeholder="Nouveau mot de passe">
                                <input type="password" class="form-control mt-2" id="passwordConfirm" name="passwordConfirm" placeholder="Confirmation du mot de passe">
                            </div>
                            <div class="row mt-5">
                                <div class="col d-flex justify-content-center">
                                    <button type="button" id="btn-change" class="btn btn-outline-light text-uppercase l-sp-5">Changer mot de passe</button>
                                </div>
                            </div>
                        </div>
                    </form>



                    <form id="form-verify" action="">
                        <span id="indication" class="text-white mt-2 mb-4 d-flex justify-content-center ">Merci de saisir le code à 6 lettres/chiffres reçu via mail.</span>
                        <div class="col d-flex justify-content-center container-form">
                            <input type="text" class="form-control input-size" id="number-1" name="number-1" maxlength="1">
                            <input type="text" class="form-control input-size" id="number-2" name="number-2" maxlength="1">
                            <input type="text" class="form-control input-size" id="number-3" name="number-3" maxlength="1">
                            <input type="text" class="form-control input-size" id="number-4" name="number-4" maxlength="1">
                            <input type="text" class="form-control input-size" id="number-5" name="number-5" maxlength="1">
                            <input type="text" class="form-control input-size" id="number-6" name="number-6" maxlength="1">
                        </div>
                        <div class="row mt-5">
                            <div class="col d-flex justify-content-center">
                                <button type="button" id="btn-envoyer" class="btn btn-outline-light text-uppercase l-sp-5">Vérification</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

</body>

<footer class="fixed-bottom bg-cyan">
    <em class="text-white">&copy; 2021</em>
</footer>
<script src="https://kit.fontawesome.com/4013fbf04f.js" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>


<script>
    $("document").ready(function() {

        var container = document.getElementsByClassName("container-form")[0];

        container.onkeyup = function(e) {

            var target = e.srcElement;
            var maxLength = parseInt(target.attributes["maxlength"].value, 5);

            var myLength = target.value.length;
            if (myLength >= maxLength) {
                var next = target;
                while (next = next.nextElementSibling) {
                    if (next == null)
                        break;
                    if (next.tagName.toLowerCase() == "input") {
                        next.focus();
                        break;
                    }
                }
                
            }
        }

        $("#btn-envoyer").on("click", function(e) {

            e.preventDefault();

            var form = document.getElementById("form-verify");
            var data = new FormData(form);
            var formData = new FormData();
            var code = "";

            for (var value of data.values()) {
                code += value
            }

            var url = $(location).attr('href');
            var segments = url.split('/');
            var token = segments[5];

            formData.append("code", code);
            formData.append("token", token)

            fetch("http://lebonkoin.localhost/index.php/UserController/verifyCode", {
                method: "POST",
                body: formData,
                type: "json"
            }).then(response => response.json()).then(formData => {
                if (formData == "verifyCodeOk") {
                    $("#form-verify").remove();
                    $("#message").addClass("d-none");
                    $("#form-change-password").removeClass("d-none");
                }
                if (formData == "verifyCodeNotOk") {
                    $("#message").removeClass("d-none");
                    $("#message").text("Merci de saisir le bon code.")
                }
            }).catch((error) => {

            })
        });

        $("#btn-change").on("click", function(e) {

            e.preventDefault();

            var form = document.getElementById("form-change-password");
            var data = new FormData(form);

            var url = $(location).attr('href');
            var segments = url.split('/');
            var token = segments[5];

            data.append("token", token)

            fetch("http://lebonkoin.localhost/index.php/UserController/changePassword", {
                method: "POST",
                body: data,
                type: "json"
            }).then(response => response.json()).then(data => {
                if (data == "changePasswordOk") {
                    $("#message").removeClass("d-none");
                    $("#message").removeClass("alert-danger");
                    $("#message").addClass("alert-success");
                    $("#message").text("Changement de mot de passe effectué avec succès vous allez être redirigé.")

                    var url = 'window.location.replace("http://lebonkoin.localhost/accueil");';
                    setTimeout(url, 1000);

                }
                if (data == "changePasswordNotOk") {
                    $("#message").removeClass("d-none");
                    $("#message").text("Les mots de passe ne corresponde pas.")
                }
                if (data == "error") {
                    $("#message").removeClass("d-none");
                    $("#message").text("Une erreur est survenue merci de réessayer.")
                }
            }).catch((error) => {

            })
        });

        // TODO

    });
</script>

</html>