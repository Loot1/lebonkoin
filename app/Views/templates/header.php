
<!doctype html>
<html lang="fr" dir="ltr">

<head>
    <title>Lebonkoin</title>
    <meta charset="utf-8">
    <meta name="author" content="Vincent Mancheron, Guillaume David, Lucas Lévêque, Louis Sachy">
    <meta name="description" content="Projet clone de Leboncoin afin de réviser les bases et de s'initier pour la plupart à CodeIgniter, un framework PHP." />
    <meta name="language" content="fr-FR">
    <meta name="subject" content="Projet clone de Leboncoin.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= base_url("public/img/icon.webp") ?>" />
    <link rel="stylesheet" href="<?= base_url("public/css/header.css") ?>">
    <link rel="stylesheet" href="<?= base_url("public/css/articles-list.css") ?>">
    <link rel="stylesheet" href="<?= base_url("public/css/home.css") ?>">
    <link rel="stylesheet" href="<?= base_url("public/css/bootstrap/bootstrap.min.css") ?>">
    <link rel="stylesheet" href="<?= base_url("public/css/DataTables/jquery.dataTables.min.css") ?>">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-cyan" id="navbar">
        <div class="navbar-brand col-6 col-md-2">
            <a href="/">
                <img src="<?= base_url("public/img/logo.webp") ?>" alt="Logo du site Lebonkoin" width="100%" height="100%">
            </a>
        </div>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse row-fluid text-center justify-content-center justify-content-between" id="navbarResponsive">
            <div class="row align-self-center m-0">
                <?php if (!empty($_SESSION['user'])) : ?>
                    <div class="col-12 col-lg-6 nav-item text-white custom-link d-flex flex-column align-items-center">
                        <a href="\creer" class="text-decoration-none text-white custom-link">
                            <i class="far fa-plus-square p-2"></i>
                            <span>Déposer une annonce</span>
                        </a>
                        <div class="divider d-lg-none"></div>
                    </div>
                <?php endif ?>
                
                <div class="col-12 col-lg-<?= $auth = !empty($_SESSION['user']) ? '6' : '12'; ?> nav-item text-white">
                    <a class="text-decoration-none text-white" href="/recherche/categorie-0">
                        <i class="fas fa-search p-2"></i>
                        <span>Faire une recherche</span>
                    </a>
                </div>
            </div>

            <?php if (!empty($_SESSION['user'])) { ?>
                <!-- Example single danger button -->
                <div class="btn-group me-3">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-bs-toggle="dropdown">Mon compte</button>
                    <ul class="dropdown-menu">
                        <?php if (!empty($_SESSION['user']['role']) && $_SESSION['user']['role'] === '1') { ?>
                            <li><a class="dropdown-item" href="/admin">Pannel admin</a></li>
                        <?php  } else { ?>
                            <li><a class="dropdown-item" href="/profil">Voir mon profil</a></li>
                        <?php } ?>
                        <li><a class="dropdown-item" href="/deconnexion">Déconnexion</a></li>
                    </ul>
                </div>
            <?php } else { ?>
                <div class="col-8 col-lg-6 nav-item d-flex justify-content-start justify-content-lg-end me-3">
                    <button type="button" class="btn bg-cyan text-white py-0 ps-0" data-bs-toggle="modal" data-bs-target="#modal-form">
                        <i class="fas fa-user p-2"></i>
                        <span>Se connecter/S'inscrire</span>
                    </button>
                </div>
            <?php }  ?>
        </div>
    </nav>

    <!-- Modal Connexion/Inscription -->
    <div class="modal fade" id="modal-form" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title-form">Se connecter</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="message" class="alert alert-primary d-none d-flex justify-content-center" role="alert"></div>
                    <form id="form-signIn" action="" method="POST">
                        <div class="mb-3 d-none" id="input-username">
                            <label for="recipient-name" class="col-form-label">Nom d'utilisateur :</label>
                            <input type="text" class="form-control" id="username" name="username" autocomplete="username">
                            <div id="sign-form-username-validator" class="form-text text-danger"></div>
                        </div>
                        <div class="mb-3">
                            <label for="recipient-name" class="col-form-label">Adresse mail :</label>
                            <input type="email" class="form-control" id="mail-adress" name="email" autocomplete="email">
                            <div id="sign-form-email-validator" class="form-text text-danger"></div>
                        </div>
                        <div class="mb-3">
                            <label for="recipient-name" class="col-form-label">Mot de passe :</label>
                            <input type="password" class="form-control" id="password" name="password" autocomplete="current-password">
                            <div id="sign-form-password-validator" class="form-text text-danger"></div>
                        </div>
                        <a class="d-flex justify-content-center" id="change-form">Pas encore inscrit ? ;o</a>
                        <a class="d-flex justify-content-center" id="forgotPassword" data-bs-toggle="modal" data-bs-target="#modal-forgotPassword">Mot de passe oublié ?</a>
                        <div class="row mt-3">
                            <div class="col d-flex justify-content-center">
                                <button type="submit" class="btn btn-success" id="form-btn" disabled>Se connecter</button>
                            </div>
                            <div class="col d-flex justify-content-center">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Mot de passe oublié -->
    <div class="modal fade" id="modal-forgotPassword" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Mot de passe oublié ?</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="message-forgotPassword" class="alert alert-danger d-none text-center" role="alert">
                    </div>
                    <form action="" id="form-forgotPassword" method="POST">
                        <div class="row">
                            <div class="mb-3">
                                <label for="input-mail" class="form-label">Adresse mail</label>
                                <input type="email" class="form-control" id="input-mail" name="email" aria-describedby="emailHelp" autocomplete="email">
                                <div id="emailHelp" class="form-text">Merci de saisir l'adresse mail utilisée lors de votre inscription.</div>
                                <div id="password-forget-form-email-validator" class="form-text text-danger"></div>
                            </div>
                        </div>

                        <div class="modal-footer justify-content-center flex-column">
                            <div class="row">
                                <a data-bs-toggle="modal" data-bs-target="#modal-form">Voulez-vous vous connecter ? / ou vous inscrire ?</a>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <button type="button" class="btn btn-success" id="change-password" disabled>Changer</button>
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <svg class="wave-header" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path fill="#00838d" fill-opacity="1" d="M0,256L30,240C60,224,120,192,180,181.3C240,171,300,181,360,186.7C420,192,480,192,540,202.7C600,213,660,235,720,250.7C780,267,840,277,900,245.3C960,213,1020,139,1080,133.3C1140,128,1200,192,1260,218.7C1320,245,1380,235,1410,229.3L1440,224L1440,0L1410,0C1380,0,1320,0,1260,0C1200,0,1140,0,1080,0C1020,0,960,0,900,0C840,0,780,0,720,0C660,0,600,0,540,0C480,0,420,0,360,0C300,0,240,0,180,0C120,0,60,0,30,0L0,0Z"></path>
    </svg>
    <div class="container-fluid">