</div>
<svg class="wave-footer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
    <path fill="#00838d" fill-opacity="1" d="M0,256L48,245.3C96,235,192,213,288,213.3C384,213,480,235,576,213.3C672,192,768,128,864,133.3C960,139,1056,213,1152,234.7C1248,256,1344,224,1392,208L1440,192L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
</svg>
<footer class="bg-cyan row align-items-center pt-4">
    <div class="col-6 g-0">
        <div class="row justify-content-center">
            <div class="col-6 d-flex flex-column">
                <a href="\Home\mentions" class="text-white text-decoration-none link">
                    <p>Mentions légales</p>
                </a>
                <a href="\Home\cgucgv" class="text-white text-decoration-none link">
                    <p>Conditions générales d'utilisation</p>
                </a>
                <a href="\Home\rgpd" class="text-white text-decoration-none link">
                    <p>Politique de confidentialité</p>
                </a>
            </div>
        </div>
    </div>
    <div class="col-6 g-0">
        <form action="" method="POST" id="form-contact">
            <div id="alert-contact-form" class="alert alert-primary d-none d-flex justify-content-center w-75" role="alert">
                Fill
            </div>
            <div class="mb-3">
                <label for="contact-form-input-mail" class="form-label text-white">Adresse mail</label>
                <input type="email" class="form-control w-75" id="contact-form-input-mail" name="email" placeholder="Adresse mail" autocomplete="email">
                <div id="contact-form-email-validator" class="form-text text-danger"></div>
            </div>
            <div class="mb-3">
                <label for="contact-form-input-message" class="form-label text-white">Message</label>
                <textarea class="form-control w-75" id="contact-form-input-message" name="message" placeholder="Laisser votre message ici" id="floatingTextarea" autocomplete="off"></textarea>
                <div id="contact-form-message-validator" class="form-text text-danger"></div>
            </div>
            <button type="submit" class="btn bg-cyan text-white" id="form-btn-contact" disabled>Envoyer</button>
        </form>
    </div>
    <span class="text-white text-center d-flex justify-content-center py-3">Fait avec 💗 par Vincent Mancheron | Guillaume David | Lucas Lévêque | Louis Sachy</span>
</footer>
<script src="https://kit.fontawesome.com/4013fbf04f.js" crossorigin="anonymous"></script>
<script src="<?= base_url("public/js/jQuery/jquery-3.6.0.min.js") ?>"></script>
<script src="<?= base_url("public/js/bootstrap/bootstrap.min.js") ?>"></script>
<script src="<?= base_url("public/js/DataTables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= base_url("public/js/header.js") ?>"></script>
<script src="<?= base_url("public/js/inscription-connexion.js") ?>"></script>
<script src="<?= base_url("public/js/contactForm.js") ?>"></script>
<script src="<?= base_url("public/js/admin.js") ?>"></script>
<script src="<?= base_url("public/js/contactFormValidator.js") ?>"></script>
<script src="<?= base_url("public/js/signFormValidator.js") ?>"></script>
<script src="<?= base_url("public/js/passwordForgetValidator.js") ?>"></script>

</body>

</html>