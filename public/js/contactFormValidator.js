var contactFormButton = document.getElementById("form-btn-contact")
const regex1 = /[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/gm

var contactFormInputEmail = document.getElementById("contact-form-input-mail")
var contactFormTextEmail = document.getElementById("contact-form-email-validator")
var contactFormInputMessage = document.getElementById("contact-form-input-message")
var contactFormTextMessage = document.getElementById("contact-form-message-validator")

function badContactForm(text,message) {
    if(text.innerHTML !== message) text.innerHTML = message
    if(contactFormButton.disabled === false) contactFormButton.disabled = true
}

function goodContactFormCheck() {
    if(contactFormInputEmail.value.length > 0) {
        if(regex1.test(contactFormInputEmail.value)) {
            if(contactFormInputMessage.value.length > 0) {
                if(contactFormButton.disabled === true) contactFormButton.disabled = false
            }
        }
    }
}

contactFormInputEmail.addEventListener("keyup", function () {
    if(contactFormInputEmail.value.length > 0) {
        if(regex1.test(contactFormInputEmail.value)) {
            if(contactFormTextEmail.innerHTML !== "") contactFormTextEmail.innerHTML = ""
            goodContactFormCheck()
        } else badContactForm(contactFormTextEmail,"Cette adresse mail n'est pas valide.")
    } else badContactForm(contactFormTextEmail,"Ce champ ne peut pas être vide.")
})

contactFormInputMessage.addEventListener("keyup", function () {
    if(contactFormInputMessage.value.length > 0) {
        if(contactFormTextMessage.innerHTML !== "") contactFormTextMessage.innerHTML = ""
        goodContactFormCheck()
    } else badContactForm(contactFormTextMessage,"Ce champ ne peut pas être vide.")
})