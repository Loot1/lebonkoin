var signFormButton = document.getElementById("form-btn")
const regex3 = /[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/gm
const regex4 = /^[a-zA-Z0-9-]+$/

var signFormInputEmail = document.getElementById("mail-adress")
var signFormTextEmail = document.getElementById("sign-form-email-validator")
var signFormInputPassword = document.getElementById("password")
var signFormTextPassword = document.getElementById("sign-form-password-validator")
var signFormInputUsername = document.getElementById("username")
var signFormTextUsername = document.getElementById("sign-form-username-validator")

function badSignForm(text, message) {
    if (text.innerHTML !== message) text.innerHTML = message
    if (signFormButton.disabled === false) signFormButton.disabled = true
}

function goodSignFormCheck() {
    if (signFormInputEmail.value.length > 0) {
        if (regex3.test(signFormInputEmail.value)) {
            if (signFormInputPassword.value.length > 0) {
                if (signFormInputPassword.value.length < 51) {
                    if (document.getElementById("form-signUp")) {
                        if (signFormInputUsername.value.length > 0) {
                            if (signFormInputUsername.value.length > 1 && signFormInputUsername.value.length < 51) {
                                if (regex4.test(signFormInputUsername.value)) {
                                    if (signFormButton.disabled === true) signFormButton.disabled = false
                                }
                            }
                        }
                    } else signFormButton.disabled = false
                }
            }
        }
    }
}

signFormInputEmail.addEventListener("keyup", function () {
    if (signFormInputEmail.value.length > 0) {
        if (regex3.test(signFormInputEmail.value)) {
            if (signFormTextEmail.innerHTML !== "") signFormTextEmail.innerHTML = ""
            goodSignFormCheck()
        } else badSignForm(signFormTextEmail, "Cette adresse mail n'est pas valide.")
    } else badSignForm(signFormTextEmail, "Ce champ ne peut pas être vide.")
})

signFormInputPassword.addEventListener("keyup", function () {
    if (signFormInputPassword.value.length > 0) {
        if (signFormInputPassword.value.length < 51) {
            if (signFormTextPassword.innerHTML !== "") signFormTextPassword.innerHTML = ""
            goodSignFormCheck()
        } else badSignForm(signFormTextPassword, "Le mot de passe ne peut pas excéder 50 caractères.")
    } else badSignForm(signFormTextPassword, "Ce champ ne peut pas être vide.")
})

signFormInputUsername.addEventListener("keyup", function () {
    if (signFormInputUsername.value.length > 0) {
        if (signFormInputUsername.value.length > 1 && signFormInputUsername.value.length < 51) {
            if (regex4.test(signFormInputUsername.value)) {
                if (signFormTextUsername.innerHTML !== "") signFormTextUsername.innerHTML = ""
                goodSignFormCheck()
            } else badSignForm(signFormTextUsername, "Le nom d'utilisateur ne doit contenir que des caractères alphanumériques.")
        } else badSignForm(signFormTextUsername, "Le nom d'utilisateur doit être compris entre 2 et 50 caractères.")
    } else badSignForm(signFormTextUsername, "Ce champ ne peut pas être vide.")
})