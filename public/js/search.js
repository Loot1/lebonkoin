// Déclaration de la fonction de réaffichage:
const redropList = (response) => {
    response.forEach(item => {
        // On check si l'image pour le produit existe:
        function checkFileExist(urlToFile) {
            var xhr = new XMLHttpRequest();
            xhr.open('HEAD', urlToFile, false);
            xhr.send();
            if (xhr.status == 404) {
                return false;
            } else {
                return true;
            }
        }
        if (checkFileExist(`/uploads/${item.id}${item.extension}`)) {
            var imgSrc = item.id+item.extension
        } else {
            var imgSrc = 'default.jpg'
        }

        // On repush la card:
        articlesList.innerHTML +=
            `<div class="card mb-3" style="width: 90%;">
                <div class="row g-0">
                    <div class="col-md-4">
                        <img src="http://localhost:8080/lebonkoin/public/uploads/${imgSrc}" class="img-fluid rounded-start" alt="Illustration de l'annonce ${item.title}">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">${item.title}</h5>
                            <p class="card-text mb-0">${item.desc}</p>
                            <p class="card-text"><small class="text-muted">Ajouté le ${item.create_at}</small></p>
                            <div class="d-flex">
                                <p class="me-2">${item.price} €</p>
                                <a href="/ArticleController/getSingle/${item.id}" class="btn btn-primary ms-2">Voir</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`
    });
}

// Déclaration du fetch adaptable:
const fetchData = (searchType) => {
    var categoryToGet = category.value == '' ? location.pathname.split('-')[1] : category.value;
    var articleNameToGet = articleName.value == '' ? 0 : articleName.value;
    var minPriceToGet = minPrice.value == '' ?  0 : minPrice.value;
    var maxPriceToGet = maxPrice.value == '' ?  0 : maxPrice.value;
    var regionToGet = region.value == '' ? 0 : region.value;

    var settings = `${categoryToGet}/${articleNameToGet}/${minPriceToGet}/${maxPriceToGet}/${regionToGet}`

    articlesList.innerHTML = '';
    fetch(`/ArticleController/getAllBySearch/${settings}`)
        .then(function (response) {
            return response.json();
        })
        .then(function (response) {
            redropList(response);
        })
}

// Déclaration des events:
const fieldsArray = [category, articleName, minPrice, maxPrice, region];
fieldsArray.forEach(input => input.onchange = () => fetchData());