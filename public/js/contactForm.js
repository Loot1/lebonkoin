$("document").ready(function () {
    $("#form-btn-contact").on("click", function (e) {
        e.preventDefault()
        let form = document.getElementById("form-contact")
        let data = new FormData(form)
        let msg = $("#alert-contact-form")
        fetch("http://lebonkoin.localhost/index.php/Home/contactForm", {
            method: "POST",
            body: data,
            type: "json"
        }).then(response => response.json()).then(data => {
            if (data == "contactFormOk") {
                $(msg).removeClass("d-none")
                $(msg).removeClass("alert-danger")
                $(msg).addClass("alert-success")
                $(msg).text("Nous avons bien reçu votre message, une copie de votre message a été envoyée sur votre adresse mail !")
            } else if (data == "formNotOk") {
                $(msg).removeClass("d-none")
                $(msg).removeClass("alert-primary")
                $(msg).addClass("alert-danger")
                $(msg).text("Merci de remplir le formulaire correctement !")
            }
        }).catch(() => {
            $(msg).removeClass("d-none")
            $(msg).removeClass("alert-primary")
            $(msg).addClass("alert-danger")
            $(msg).text("Une erreur innatendue s'est produite, veuillez réessayer plus tard")
        })
    })
})
