$("document").ready(function () {
    $("#change-form").on("click", function () {
        if ($("#title-form").text() == "Se connecter") {
            $("form").attr("id", "form-signIn");
            $("#password").attr("autocomplete", "current-password");
        } else if ($("#title-form").text() == "S'inscrire") {
            $("form").attr("id", "form-signUp");
            $("#password").attr("autocomplete", "new-password");
        }
    });

    $("#form-btn").on("click", function (e) {
        e.preventDefault();

        if ($("form").attr("id") == "form-signUp") {

            var form = document.getElementById("form-signUp");
            var data = new FormData(form);
            var message = $("#message");


            fetch("http://lebonkoin.localhost/index.php/UserController/signUp", {
                method: "POST",
                body: data,
                type: "json"
            }).then(response => response.json()).then(data => {
                if (data == "emailNotOk") {
                    $(message).removeClass("d-none");
                    $(message).removeClass("alert-primary");
                    $(message).addClass("alert-danger");
                    $(message).text("L'adresse mail existe, merci de vous connectez.");
                }
                if (data == "pseudoNotOk") {
                    $(message).removeClass("d-none");
                    $(message).removeClass("alert-primary");
                    $(message).addClass("alert-danger");
                    $(message).text("Veuillez choisir un autre pseudo.");
                }
                if (data == "signUpOk") {
                    $(message).removeClass("d-none");
                    $(message).removeClass("alert-danger");
                    $(message).addClass("alert-success");
                    $(message).text("Vous êtes inscrit, merci de vérifier votre adresse mail");
                }
                if (data == "formNotOk") {
                    $(message).removeClass("d-none");
                    $(message).removeClass("alert-primary");
                    $(message).addClass("alert-danger");
                    $(message).text("Merci de remplir le formulaire correctement !");
                }
            }).catch((error) => {

            })
        } else {

            var form = document.getElementById("form-signIn");
            var data = new FormData(form);
            var message = $("#message");

            fetch("http://lebonkoin.localhost/index.php/UserController/signIn", {
                method: "POST",
                body: data,
                type: "json"
            }).then(response => response.json()).then(data => {
                if (data == "signInOk") {
                    $(message).removeClass("d-none");
                    $(message).removeClass("alert-primary");
                    $(message).addClass("alert-success");
                    $(message).text("Connexion effectuée vous allez être redirigé.");
                    window.location.replace("http://lebonkoin.localhost/profil");
                }
                if (data == "accountNoVerify") {
                    $(message).removeClass("d-none");
                    $(message).removeClass("alert-primary");
                    $(message).addClass("alert-warning");
                    $(message).text("Votre compte n'est pas vérifié merci de l'effectué.");
                }
                if (data == "mailNoExist") {
                    $(message).removeClass("d-none");
                    $(message).removeClass("alert-primary");
                    $(message).addClass("alert-danger");
                    $(message).text("L'adresse mail n'existe pas");
                }
            }).catch((error) => {

            })
        }



    });

    $("#change-password").on("click", function (e) {
        e.preventDefault();

        var form = document.getElementById("form-forgotPassword");
        var data = new FormData(form);
        var message = $("#message-forgotPassword");

        fetch("http://lebonkoin.localhost/index.php/UserController/forgotPassword", {
            method: "POST",
            body: data,
            type: "json"
        }).then(response => response.json()).then(data => {
            if (data == "emailOk") {
                message.removeClass("d-none");
                message.removeClass("alert-danger");
                message.addClass("alert-success");
                message.text("Vous allez recevoir un mail afin de modifier votre mot de passe.");
            }
            if (data == "emailNotExist") {
                message.removeClass("d-none");
                message.text("L'adresse email n'existe pas.");
            }
        }).catch((error) => {

        })

    })



});