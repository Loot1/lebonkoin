var passwordForgetFormButton = document.getElementById("change-password")
const regex2 = /[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/gm

var passwordForgetFormInputEmail = document.getElementById("input-mail")
var passwordForgetFormTextEmail = document.getElementById("password-forget-form-email-validator")

function badPasswordForgetForm(text,message) {
    if(text.innerHTML !== message) text.innerHTML = message
    if(passwordForgetFormButton.disabled === false) passwordForgetFormButton.disabled = true
}

passwordForgetFormInputEmail.addEventListener("keyup", function () {
    if(passwordForgetFormInputEmail.value.length > 0) {
        if(regex2.test(passwordForgetFormInputEmail.value)) {
            if(passwordForgetFormTextEmail.innerHTML !== "") passwordForgetFormTextEmail.innerHTML = ""
            if(passwordForgetFormButton.disabled === true) passwordForgetFormButton.disabled = false
        } else badPasswordForgetForm(passwordForgetFormTextEmail,"Cette adresse mail n'est pas valide.")
    } else badPasswordForgetForm(passwordForgetFormTextEmail,"Ce champ ne peut pas être vide.")
})