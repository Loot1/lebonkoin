$("document").ready(() => {
    $("#change-form").on("click", () => {
        if ($("#title-form").text() == "Se connecter") {
            $("#input-username").removeClass("d-none")
            $("#change-form").text("Vous avez déjà un compte ?")
            $("#form-btn").text("S'inscrire")
            $("#title-form").text("S'inscrire")
        } else {
            $("#input-username").addClass("d-none")
            $("#change-form").text("Pas encore inscrit ? ;o")
            $("#form-btn").text("Se connecter")
            $("#title-form").text("Se connecter")
        }
    })
})