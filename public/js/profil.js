let penRegion = document.getElementById('modify')
let region = document.getElementById('region')
let FormRegion = document.getElementById('FormRegion')
penRegion.addEventListener('click', function () {
    if (FormRegion.classList.contains('d-none')) {
        FormRegion.classList.remove('d-none');
        penRegion.innerHTML = 'Annuler <i class="fas fa-sm fa-times"></i>'
        penRegion.classList.remove('btn-warning')
        penRegion.classList.add('btn-danger')
    } else {
        FormRegion.classList.add('d-none');
        penRegion.innerHTML = 'Modifier <i class="fas fa-sm fa-pen"></i>'
        penRegion.classList.remove('btn-danger')
        penRegion.classList.add('btn-warning')
    }
})