<?php
// Déclaration des Regex:
// define('REGALPHA','[a-zA-Zéèçàùâêîûüëï ,\'"]+');
// define('REGALPHANUM','[a-zA-Z0-9éèçàùâêîûüëï ,\'"]+');
// ~!#$%&*-_+=|:.
define('REGALPHANUMSPECHARS','[a-zA-Z0-9àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ\'()& -!?,.;#$+-=:]+');
// define('REGBIRTHDATE','[0-9\/]+');
define('REGNUM','\d*');
// define('REGEMAIL','[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}');
// define('REGPASSWORD','^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$');
// define('REGTEL','(0|\+33)[1-9]( *[0-9]{2}){4}');
// define('REGZIP','\d{5}');
// define('REGCHECKBOX','1');