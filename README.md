# Projet Lebonkoin

## Synthèse

Le but du projet était de concevoir un clone du site [Leboncoin](https://www.leboncoin.fr/) en 5 jours (du 11/10/2021 au 15/10/2021) au moyen de [Code Igniter 4](http://codeigniter.com).
Le groupe est constitué de Vincent Mancheron, Guillaume David, Lucas Lévêque et Louis Sachy.